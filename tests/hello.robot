*** Settings ***
Library           SeleniumLibrary
Documentation     Test One Money functions.

*** Variables ***
${LOGIN URL}      https://www.google.com/
${BROWSER}        chrome
${MOBILE}         8979900301


*** Keywords ***
Open Browser to Landing Page
    Open Browser  ${LOGIN URL}  ${BROWSER}  options=add_argument("--headless");add_argument("--no-sandbox");add_argument("--disable-dev-shm-usage")
Submit Info
    Click Button  submit
Then I should see the OTP Page
    Wait Until Page Contains  hello  timeout=None  error=None


*** Test Cases ***
Invalid User Tries to Login IPFM
    Open Browser to Landing Page
    Close Browser


