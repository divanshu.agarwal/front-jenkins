import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Subject } from 'rxjs/internal/Subject';
import { map } from 'rxjs/operators';

interface Post{
    title: string;
    content: string;
}

interface Response{
    id: string,
    title: string;
    content: string;
}



@Injectable({providedIn: 'root'})
export class PostService {
    error = new Subject<string>()
    constructor(private http: HttpClient) { }
    
    createPost(title: string, content: string){
        const postData: Post = {title, content}
        return this.http
            .post<{name: string}>('https://http-16019-default-rtdb.firebaseio.com/posts.json/',postData)
            .subscribe(
                res => console.log(res),
                err => {
                    console.log(this.error)
                    this.error.next(err.message)
                }
            )
    }


    fetchPosts(): Observable<Response[]> {
        return this.http
        .get<{[key: string]: Post}>('https://http-16019-default-rtdb.firebaseio.com/posts.json')
        .pipe(map(responseData => {
          const posts: Response[] = [];
          for(const [key,value] of Object.entries(responseData)){
            posts.push({
                id: key,
                ...value
            })
          }
          return posts;
        }))
    }

    clearPosts(){
        return this.http.delete('https://http-16019-default-rtdb.firebaseio.com/posts.json')
    }
}