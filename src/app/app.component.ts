import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Subject, Subscription } from 'rxjs';
import { PostService } from './post.service';

interface Post{
  id: string;
  title: string;
  content: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  isPostLoading = false;
  loadedPosts: Post[] = [];
  error = "";
  errorSub: Subscription
  title = 'divanshu'

  constructor(private http: HttpClient,private postService: PostService) {}

  ngOnInit() {
    this.onFetchPosts()
    this.errorSub = this.postService.error.subscribe(
      errMsg => this.error = errMsg
    )
  }

  ngOnDestroy(): void {
    this.errorSub.unsubscribe()
  }

  onCreatePost(postData: { title: string; content: string }) {
    this.postService.createPost(postData.title,postData.content)
  }

  onFetchPosts() {
    this.postService.fetchPosts().subscribe(
      resData => {
        this.loadedPosts = resData
      },
      err => this.error = err.message
    )
  }

  onClearPosts() {
    this.postService.clearPosts().subscribe(
      _ => {
        this.loadedPosts = []
      }
    )
  }
}
